import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

/**
 * @author Tim Plummer
 * @partner Ian Smith
 * Version 4 of the Population Query implementation
 * This implementation uses the ForkJoin framework
 * to preprocess and query the data in parallel
 * also uses the Version 3 method of querying
 */
public class Version4 extends Version3
{
	/**
	 * Cutoff values for the fork join tasks
	 */
	public static int COMPUTE_CUTOFF = 5000;
	public static int COMBINE_CUTOFF = 5000;
	
	private ForkJoinPool pool;

	/**
	 * Version 4 constructor
	 * @param data Census data
	 * @param columns Grid columns
	 * @param rows Grid rows
	 */
	public Version4(CensusData data, int columns, int rows)
	{
		super(data, columns, rows);
        this.pool = new ForkJoinPool();
	}
	
	/* (non-Javadoc)
	 * @see Version3#preprocess()
	 */
	@Override
	public void preprocess()
	{
		Version2.PreprocessTask task = new Version2.PreprocessTask(data.data, 0, data.data_size);
        Pair<Rectangle, Integer> result = pool.invoke(task);
        this.edges = result.getElementA();
        this.totalPop = result.getElementB();
        
        accumulatedPopGrid = calculatePopGridParallel();
		calculateAccumulatedPopGrid(accumulatedPopGrid);
	}

	/**
	 * Populates the preprocessed population grid in parallel
	 */
	private class PopGridTask extends RecursiveTask<int[][]>
	{
		private static final long serialVersionUID = 3190583102231668098L;
		
		private int lo;
		private int hi;

		/**
		 * @param lo Lower bound of data
		 * @param hi Upper bound of data
		 */
		public PopGridTask(int lo, int hi)
		{
			this.hi = hi;
			this.lo = lo;
		}
		
		@Override
		protected int[][] compute()
		{
			if((hi-lo) < COMPUTE_CUTOFF)
			{
				int[][] grid = new int[columns][rows];
				for(int i=lo; i<hi; i++)
				{
					CensusGroup g = data.data[i];
		            //subtract 1 from toGrid() answer because our array is 0-indexed
		            int x = toGrid(g.longitude, edges.left, edges.right, columns) - 1;
		            int y = toGrid(g.latitude, edges.bottom, edges.top, rows) - 1;
		            grid[x][y] += g.population;
				}
				return grid;
			}
			
			int mid = (lo+hi)/2;
			
			PopGridTask left = new PopGridTask(lo, mid);
			
			left.fork();
			int[][] rightResult = new PopGridTask(mid, hi).compute();
			int[][] leftResult = left.join();
			
			CombineTask task = new CombineTask(leftResult, rightResult, 0, leftResult.length);
			return pool.invoke(task);
		}
		
	}

	/**
	 * Combines the two result arrays from the PopGridTask computation
	 */
	private class CombineTask extends RecursiveTask<int[][]>
	{
		private static final long serialVersionUID = -298556560535485853L;
		private int[][] left;
		private int[][] right;
		private int lo;
		private int hi;
		
		/**
		 * Combine task constructor
		 * @param left Left grid to combine (this is the array that is returned
		 * @param right Right grid to add to left
		 * @param lo lower bound
		 * @param hi upper bound
		 */
		public CombineTask(int[][] left, int[][] right, int lo, int hi)
		{
			this.left = left;
			this.right = right;
			this.lo = lo;
			this.hi = hi;
		}
		
		@Override
		protected int[][] compute()
		{
			if((hi-lo) < COMBINE_CUTOFF)
			{
				for(int x=lo; x<hi; x++)
				{
					for(int y=0; y<left[x].length; y++)
					{
						left[x][y] += right[x][y];
					}
				}
				return left;
			}
			
			int mid = (lo+hi)/2;
			CombineTask leftTask = new CombineTask(left, right, lo, mid);
			CombineTask rightTask = new CombineTask(left, right, mid, hi);
			
			leftTask.fork();
			rightTask.compute();
			leftTask.join();

			return left;
		}
	}
	
	/**
	 * Replaces the calculatePopGrid method in version 3.
	 * We need to return a value instead of use an already instantiated one, 
	 * because each parallel computation creates a new grid anyways
	 * @return Populated population grid
	 */
	protected int[][] calculatePopGridParallel()
	{
		PopGridTask task = new PopGridTask(0, data.data_size);
		return pool.invoke(task);
	}

}
