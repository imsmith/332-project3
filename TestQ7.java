/**
 * Test file for answering Question 7 in the writeup.
 * @author Ian Smith
 * @partner
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestQ7 {
    public CensusData data2010;

    private final int MAX_GRID_SIZE = 1500;
    private final int GRID_DELTA = 10;
    private final int MAX_RUNS = 60;
    // runs to warm up jvm. Make sure THROW_AWAY_RUNS < MAX_RUNS, or else no times will be output!
    private final int THROW_AWAY_RUNS = 10;

    private enum VERSION {
        ONE, TWO, THREE, FOUR, FIVE
    }


    @Before
    public void setup() {
        // Parse the files
        data2010 = PopulationQuery.parse("CenPop2010.txt");

    }

    @Test
    public void testVersion4() {
        // Time avg version creation and preprocess only, for MAX_RUNS times (throwing away first THROW_AWAY_RUNS).
        // Each time increment the grid size (which is square), by GRID_DELTA, from 1x1 to MAX_GRID_SIZExMAX_GRID_SIZE.
        System.out.println("-= Version 4 =-\n");
        System.out.println("Gridsize (square) |  Avg time (ms)\n");
        for (int gridSize=1; gridSize<MAX_GRID_SIZE; gridSize+=GRID_DELTA) {
            long start = 0;
            for (int run=0; run<MAX_RUNS; run++){
                if (run == THROW_AWAY_RUNS) {
                    start = System.nanoTime();
                }
                Version4 v = new Version4(data2010, gridSize, gridSize);
                v.preprocess();
                v = null;
            }
            System.out.println(gridSize + "\t\t\t" + (((System.nanoTime() - start) / 1000000.0)) / (MAX_RUNS - THROW_AWAY_RUNS));
        }
    }

    @Test
    public void testVersion5() {

        // Time avg version creation and preprocess only, for MAX_RUNS times (throwing away first THROW_AWAY_RUNS).
        // Each time increment the grid size (which is square), by GRID_DELTA, from 1x1 to MAX_GRID_SIZExMAX_GRID_SIZE.
        System.out.println("-= Version 5 =-\n");
        System.out.println("Gridsize (square) |  Avg time (ms)\n");
        for (int gridSize=1; gridSize<MAX_GRID_SIZE; gridSize+=GRID_DELTA) {
            long start = 0;
            for (int run=0; run<MAX_RUNS; run++){
                start = System.nanoTime();
                if (run == THROW_AWAY_RUNS) {
                }
                Version5 v = new Version5(data2010, gridSize, gridSize);
                v.preprocess();
                v = null;
            }
            System.out.println(gridSize + "\t\t\t" + (((System.nanoTime() - start) / 1000000.0)) / (MAX_RUNS - THROW_AWAY_RUNS));

        }
    }
}