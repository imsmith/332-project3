import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * @author Tim Plummer
 * @partner Ian Smith
 * Version 2 of the Population Query implementation
 * This implementation uses the ForkJoin framework
 * to preprocess and query the data in parallel
 */
public class Version2 extends QueryVersion {
    // ForkJoin pool
    // Only one Version will be instantiated at any time, so we don't need to share this one with the others
    private ForkJoinPool pool;

    /**
     * @author Tim Plummer
     * @partner Ian Smith
     * PreprocessTask adds up the total area and total population in parallel
     */
    public static class PreprocessTask extends RecursiveTask<Pair<Rectangle, Integer>> {
        private static final long serialVersionUID = -1803674633430706013L;

        /**
         * Cutoff to stop forking new tasks
         */
        public static final int CUTOFF = 10;

        // Census group data
        private CensusGroup[] array;
        // Starting point in the array for this process (inclusive)
        private int start;
        // Ending point in the array for this process (exclusive)
        private int end;


        /**
         * PreprocessTask constructor
         *
         * @param array Data to preprocess
         * @param start Starting point in array
         * @param end   Ending point in array
         */
        public PreprocessTask(CensusGroup[] array, int start, int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }

        /* (non-Javadoc)
           * @see java.util.concurrent.RecursiveTask#compute()
           */
        @Override
        protected Pair<Rectangle, Integer> compute() {
            // Check the cutoff
            if (end - start <= CUTOFF) {
                // If less than cutoff length, sum up the rectangles and populations sequentially
                // (Same as version 1 mostly)
                CensusGroup init = array[start];
                Rectangle rect = new Rectangle(init.longitude, init.longitude,
                        init.latitude, init.latitude);
                int pop = init.population;
                for (int i = start + 1; i < end; i++) {
                    CensusGroup group = array[i];
                    rect = rect.encompass(new Rectangle(group.longitude, group.longitude,
                            group.latitude, group.latitude));
                    pop += group.population;
                }
                return new Pair<Rectangle, Integer>(rect, pop);
            }

            // If we get here, the array is larger than the cutoff
            int center = (start + end) / 2;

            // Split remaining work in half
            PreprocessTask left = new PreprocessTask(array, start, center);
            PreprocessTask right = new PreprocessTask(array, center, end);

            // Fork half the data and compute the remaining
            left.fork();
            Pair<Rectangle, Integer> rightResult = right.compute();
            Pair<Rectangle, Integer> leftResult = left.join();

            // Combine the rectangle and total population count
            return new Pair<Rectangle, Integer>(rightResult.getElementA().encompass(leftResult.getElementA()), rightResult.getElementB() + leftResult.getElementB());
        }
    }

    /**
     * @author Tim Plummer
     * @partner Ian Smith
     * QueryTask computes the query recursively and in parallel
     */
    private class QueryTask extends RecursiveTask<Integer> {
        private static final long serialVersionUID = 4988083635840314263L;

        // Cutoff to start working sequentially
        public static final int CUTOFF = 10;

        // Data to work on
        private CensusGroup[] array;
        // Starting point
        private int start;
        // Ending point
        private int end;
        // Rectangle containing the US area
        private Rectangle rect;


        /**
         * QueryTask constructor
         *
         * @param data  Data to work on
         * @param start Starting point
         * @param end   Ending point
         * @param rect  Rectangle containing US area
         */
        public QueryTask(CensusGroup[] data, int start, int end, Rectangle rect) {
            this.array = data;
            this.start = start;
            this.end = end;
            this.rect = rect;
        }

        /* (non-Javadoc)
           * @see java.util.concurrent.RecursiveTask#compute()
           */
        @Override
        protected Integer compute() {
            if (end - start <= CUTOFF) {
                // Sequentially compute the total population in this area
                // (Mostly same as Version 1)
                int queryPop = 0;
                for (int i = start; i < end; i++) {
                    CensusGroup group = array[i];
                    if (group == null) break;
                    int x = toGrid(group.longitude, edges.left, edges.right, columns);
                    int y = toGrid(group.latitude, edges.bottom, edges.top, rows);
                    if (x >= rect.left && x <= rect.right)
                        if (y >= rect.bottom && y <= rect.top)
                            queryPop += group.population;
                }
                return queryPop;
            }

            int center = (start + end) / 2;

            // Split remaining work in half
            QueryTask left = new QueryTask(array, start, center, rect);
            QueryTask right = new QueryTask(array, center, end, rect);

            // Fork half and compute the rest
            left.fork();
            Integer rightResult = right.compute();
            Integer leftResult = left.join();

            // Combine results by adding
            return rightResult + leftResult;
        }
    }

    /**
     * Construct the version class
     *
     * @param data    Raw census data
     * @param columns Number of columns in grid
     * @param rows    Number of rows in grid
     */
    public Version2(CensusData data, int columns, int rows) {
        this.data = data;
        this.columns = columns;
        this.rows = rows;
        this.pool = new ForkJoinPool();
    }

    /* (non-Javadoc)
      * @see QueryVersion#preprocess()
      */
    @Override
    public void preprocess() {
        PreprocessTask task = new PreprocessTask(data.data, 0, data.data_size);
        Pair<Rectangle, Integer> result = pool.invoke(task);
        this.edges = result.getElementA();
        this.totalPop = result.getElementB();
    }

    /* (non-Javadoc)
      * @see QueryVersion#query(int, int, int, int)
      */
    @Override
    public Pair<Integer, Float> query(int w, int s, int e, int n) {
        Rectangle rect = new Rectangle(w, e, n, s);
        int result = pool.invoke(new QueryTask(data.data, 0, data.data_size, rect));
        return new Pair<Integer, Float>(result, (float) result / (float) totalPop * 100.0f);
    }
}
