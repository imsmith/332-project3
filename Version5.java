/**
 * Version5 class extends Version3, and preprocesses the population in parallel
 * by creating threads that concurrently update a population grid. This allows for
 * faster preprocessing than Version3, but still with the O(1) queurying.
 *
 * @author Ian Smith
 * @partner Tim Plummer
 */

public class Version5 extends Version3 {
    // Version2 used for getting total population and max rectangle edges
    private Version2 v2;

    // 2-D array of lock objects that matches V3 accumulatedPopGrid elements
    private Object[][] lockArr;

    // Cutoff for the threads that preprocess the CensusData
    protected static final int SEQUENTIAL_CUTOFF = 5000;

    /**
     * Constructor
     * Creates a new empty Version3 object with an empty 2-D array the size of columns x rows.
     *
     * @param data    census data
     * @param columns number of columns in grid
     * @param rows    number of rows in grid
     */
    public Version5(CensusData data, int columns, int rows) {
        super(data, columns, rows);
        v2 = new Version2(data, columns, rows);
        lockArr = new Object[columns][rows];
    }

    /**
     * Preprocesses the census data to populate a 2-D array to allow for O(1) queries.
     */
    @Override
    public void preprocess() {
        // Use Version2 preprocess code to find totalPop and edges in parallel
        v2.preprocess();
        edges = v2.edges;
        totalPop = v2.totalPop;

        // Initialize lockArr
        initLockArr(lockArr, columns, rows);

        // Calculate the population grid in parallel using threads
        calculatePopGrid(this.accumulatedPopGrid);
        // Calculate the accumulated population to enable O(1) queries
        calculateAccumulatedPopGrid(this.accumulatedPopGrid);
    }

    /**
     * Initialize the lock array with Objects, so the elements are lockable
     *
     * @param arr     2D array
     * @param arrCols number of columns in the array
     * @param arrRows number of rows in the array
     */
    protected void initLockArr(Object[][] arr, int arrCols, int arrRows) {
        for (int i = 0; i < arrCols; i++) {
            for (int j = 0; j < arrRows; j++) {
                arr[i][j] = new Object();
            }
        }
    }

    /**
     * Fills the 2-D array concurrently with the populations at each grid division
     * with multiple threads
     *
     * @param popArr The 2-D array to populate
     */
    @Override
    protected void calculatePopGrid(int[][] popArr) {
        CalcPopulationThread t = new CalcPopulationThread(data.data, 0, data.data_size);
        t.run();
    }

    /**
     * Reads an array of census groups to set the element values of the
     * population grid
     *
     * @param arr array of censud group data
     * @param lo  lowest index in the array to read
     * @param hi  one greater than the highest index to read in the array
     *            ie. the index to read up to, but not including
     */
    protected void fillPopGrid(CensusGroup[] arr, int lo, int hi) {
        for (int i = lo; i < hi; i++) {
            CensusGroup group = arr[i];
            if (group == null) continue;
            int x = toGrid(group.longitude, edges.left, edges.right, columns) - 1;
            int y = toGrid(group.latitude, edges.bottom, edges.top, rows) - 1;
            // synchronize on the lock in the corresponding lock array
            synchronized (lockArr[x][y]) {
                accumulatedPopGrid[x][y] += group.population;
            }
        }
    }


    /**
     * Inner class of threads to calculate the population grid in parallel
     */
    protected class CalcPopulationThread extends java.lang.Thread {

        // lowest array index to read in this thread
        int lo;
        // one greater than the highest index this thread reads
        int hi;
        // array of census groups to read
        CensusGroup[] arr;

        /**
         * Constructor
         *
         * Creates a thread with array and bounds
         *
         * @param arr array of census groups
         * @param lo  lowest index in array to read
         * @param hi  one greater than the highest index this thread reads
         */
        CalcPopulationThread(CensusGroup[] arr, int lo, int hi) {
            this.arr = arr;
            this.lo = lo;
            this.hi = hi;
        }

        /**
         * Fills the population grid if the range of indexes for this thread is less
         * than the cutoff. Otherwise it makes a 2 new thread each with half the range.
         *
         * @throws RuntimeException when thread throws an threadinterrupted exception
         */
        public void run() throws RuntimeException {
            if (hi - lo < SEQUENTIAL_CUTOFF) {
                fillPopGrid(arr, lo, hi);
            } else {
                int mid = (hi + lo) / 2;
                // Create new threads
                CalcPopulationThread left = new CalcPopulationThread(arr, lo, mid);
                CalcPopulationThread right = new CalcPopulationThread(arr, mid, hi);
                // Start the left thread, and recurse on the right thread so all threads
                // do work.
                left.start();
                right.run();
                try {
                    left.join();
                } catch (Exception e) {
                    throw new RuntimeException();
                }
            }
        }
    }
}
