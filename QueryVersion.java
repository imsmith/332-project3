import javax.swing.text.html.HTMLDocument;

/**
 * @author Tim Plummer
 * @partner Ian Smith
 * Abstract QueryVersion class which each version extends to implement the query and preprocessing methods
 */
public abstract class QueryVersion {
    // Raw census data
    protected CensusData data;
    // Grid columns
    protected int columns;
    // Grid rows
    protected int rows;
    // Edges of map
    protected Rectangle edges;
    // Total population in map
    protected int totalPop;

    /**
     * Preprocesses the data to get the total population and boundary edges of the map
     */
    public abstract void preprocess();

    /**
     * Queries the data to get the population in the given rectangle and the percentage of the total
     * population this number represents
     *
     * @param w Western edge of rectangle
     * @param s Southern edge of rectangle
     * @param e Eastern edge of rectangle
     * @param n Northern edge of rectangle
     * @return Pair of population and percentage of total population (0-100)
     */
    public abstract Pair<Integer, Float> query(int w, int s, int e, int n);

    /*
      * Divides a float range (start to end) into divCount divisions, and then
      * returns the division the map lies within
      */
    public int toGrid(float map, float start, float end, int divCount) {
        // Special case when map == end, return the last division
        if (map == end) {
            return divCount;
        }

        float range = end - start;
        // Special case when there is only one census point, return 1
        if (range == 0) {
            return 1;
        }
        float div = ((map - start) / range * divCount) + 1.0f;
        // Round to North and East if it won't go out of bounds
        if (div == Math.round(div)) {
            div = Math.min(div + 1, divCount);
        }
        return (int) div;
    }
}
