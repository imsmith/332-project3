/**
 * Timing test code for writeup question 6
 *
 * @authors Ian Smith and Tim Plummer
 */

import org.junit.Before;
import org.junit.Test;

public class TestQ6 {
    public QueryVersion version;

    public CensusData data2010;
    
    private final int[] cutoffs = {5, 10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000};

    // Default rows and columns for this test
    // (the sample values in testSampleValues() depend on these numbers)
    private final int TEST_SAMPLE_COLUMNS = 20;
    private final int TEST_SAMPLE_ROWS = 25;

    @Before
    public void setup() {
        // Parse the files
        data2010 = PopulationQuery.parse("CenPop2010.txt");
    }

    @Test
    public void testVersion4() {
    	for(int i=0; i<cutoffs.length; i++)
    	{
	        version = new Version4(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
	        Version4.COMPUTE_CUTOFF = cutoffs[i];
	        long time1 = 0;
	        for(int x=0; x<100; x++)
	        {
	        	long start = System.nanoTime();
	        	version.preprocess();
	        	time1 += (System.nanoTime()-start);
	        	time1 /= 2;
	        }
	        
	        //long time2 = time100Queries(1, 1, 20, 25);
	        
	        System.out.println("\t" + cutoffs[i] + "\t\t" + time1 + "\t\t" + time1);
    	}
    }

    // Tests a single sample value against a known population and percentage
    private long time100Queries(int w, int s, int e, int n) {
        long start = System.nanoTime();
        for(int i=0; i<100; i++)
        {
        	version.query(w, s, e, n);
        }
        long time = System.nanoTime()-start;
        return time;
    }
}
