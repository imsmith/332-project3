/**
 * @partners: Ian Smith and Tim Plummer
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class PopulationQuery {

    private static CensusData data;
    private static QueryVersion queryier;

    // next four constants are relevant to parsing
    public static final int TOKENS_PER_LINE = 7;
    public static final int POPULATION_INDEX = 4; // zero-based indices
    public static final int LATITUDE_INDEX = 5;
    public static final int LONGITUDE_INDEX = 6;

    // parse the input file into a large array held in a CensusData object
    public static CensusData parse(String filename) {
        CensusData result = new CensusData();

        try {
            BufferedReader fileIn = new BufferedReader(new FileReader(filename));

            // Skip the first line of the file
            // After that each line has 7 comma-separated numbers (see constants above)
            // We want to skip the first 4, the 5th is the population (an int)
            // and the 6th and 7th are latitude and longitude (floats)
            // If the population is 0, then the line has latitude and longitude of +.,-.
            // which cannot be parsed as floats, so that's a special case
            //   (we could fix this, but noisy data is a fact of life, more fun
            //    to process the real data as provided by the government)

            String oneLine = fileIn.readLine(); // skip the first line

            // read each subsequent line and add relevant data to a big array
            while ((oneLine = fileIn.readLine()) != null) {
                String[] tokens = oneLine.split(",");
                if (tokens.length != TOKENS_PER_LINE)
                    throw new NumberFormatException();
                int population = Integer.parseInt(tokens[POPULATION_INDEX]);
                if (population != 0)
                    result.add(population,
                            Float.parseFloat(tokens[LATITUDE_INDEX]),
                            Float.parseFloat(tokens[LONGITUDE_INDEX]));
            }

            fileIn.close();
        } catch (IOException ioe) {
            System.err.println("Error opening/reading/writing input or output file.");
            System.exit(1);
        } catch (NumberFormatException nfe) {
            System.err.println(nfe.toString());
            System.err.println("Error in file format");
            System.exit(1);
        }
        return result;
    }

    // argument 1: file name for input data: pass this to parse
    // argument 2: number of x-dimension buckets
    // argument 3: number of y-dimension buckets
    // argument 4: -v1, -v2, -v3, -v4, or -v5
    public static void main(String[] args) {
        if (args.length != 4) {
            return;
        }
        String filename = args[0];
        int xBuckets = 0;
        int yBuckets = 0;
        int version = 0;
        try {
            xBuckets = Integer.parseInt(args[1]);
            yBuckets = Integer.parseInt(args[2]);
            version = Integer.parseInt("" + args[3].charAt(2));
        } catch (NumberFormatException e) {
            System.out.println("Invalid input entered: " + e.getMessage());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid input entered: " + e.getMessage());
        }

        preprocess(filename, xBuckets, yBuckets, version);

        System.out.println("Please give west, south, east, north coordinates of your query rectangle:");
        Scanner in = new Scanner(System.in);
        String[] input = in.nextLine().split(" ");
        while (input.length == 4) {
            int w = Integer.parseInt(input[0]);
            int s = Integer.parseInt(input[1]);
            int e = Integer.parseInt(input[2]);
            int n = Integer.parseInt(input[3]);
            validateInput(xBuckets, yBuckets, w, s, e, n);

            Pair<Integer, Float> result = singleInteraction(w, s, e, n);
            System.out.println("population of rectangle: " + result.getElementA());
            System.out.println(String.format("percent of total population: %1$.2f", result.getElementB()));

            System.out.println("Please give west, south, east, north coordinates of your query rectangle:");
            input = in.nextLine().split(" ");
        }
    }

    public static void preprocess(String filename, int columns, int rows, int versionNum) {
        data = parse(filename);
        switch (versionNum) {
            case 1:
                queryier = new Version1(data, columns, rows);
                break;
            case 2:
                queryier = new Version2(data, columns, rows);
                break;
            case 3:
                queryier = new Version3(data, columns, rows);
                break;
            case 4:
            	queryier = new Version4(data, columns, rows);
            	break;
            case 5:
            	queryier = new Version5(data, columns, rows);
            	break;
        }
        queryier.preprocess();
    }


    public static Pair<Integer, Float> singleInteraction(int w, int s, int e, int n) {
        return queryier.query(w, s, e, n);
    }

    private static void validateInput(int x, int y, int w, int s, int e, int n) {
        if (w < 1 || w > x) {
            invalidInput("West is not valid");
        }
        if (s < 1 || s > y) {
            invalidInput("South is not valid");
        }
        if (e < w || e > x) {
            invalidInput("East is invalid");
        }
        if (n < s || n > y) {
            invalidInput("North is invalid");
        }
    }

    private static void invalidInput(String error) {
        System.err.println("Error, invalid input: " + error);
        System.exit(1);
    }
}
