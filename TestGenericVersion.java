/**
 * Tests all versions of the QueryVersion class by comparing to known values, and
 * dividing up data into many sized grids and testing sums add up to totals.
 *
 * @authors Ian Smith and Tim Plummer
 */

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestGenericVersion {
    public QueryVersion version;

    public CensusData data2010;
    public CensusData dataSingle;

    // Default rows and columns for this test
    // (the sample values in testSampleValues() depend on these numbers)
    private final int TEST_SAMPLE_COLUMNS = 20;
    private final int TEST_SAMPLE_ROWS = 25;

    // Constants defining min and max rows and columns in testChunksSum
    private final int MIN_CHUNK_COLUMNS = 10;
    private final int MIN_CHUNK_ROWS = 10;
    private final int MAX_CHUNK_COLUMNS = 12;
    private final int MAX_CHUNK_ROWS = 12;

    private enum VERSION {
        ONE, TWO, THREE, FOUR, FIVE
    }


    @Before
    public void setup() {
        // Parse the files
        data2010 = PopulationQuery.parse("CenPop2010.txt");
        dataSingle = PopulationQuery.parse("CenPopSingle.txt");

    }

    @Test
    public void testVersion1() {
        version = new Version1(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
        version.preprocess();
        testSampleValues();

        testManyChunksSum(VERSION.ONE, dataSingle, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
        testManyChunksSum(VERSION.ONE, data2010, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
    }

    @Test
    public void testVersion2() {
        version = new Version2(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
        version.preprocess();
        testSampleValues();

        testManyChunksSum(VERSION.TWO, dataSingle, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
        testManyChunksSum(VERSION.TWO, data2010, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
    }

    @Test
    public void testVersion3() {
        version = new Version3(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
        version.preprocess();
        testSampleValues();

        testManyChunksSum(VERSION.THREE, dataSingle, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
        testManyChunksSum(VERSION.THREE, data2010, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
    }

    @Test
    public void testVersion4() {
        version = new Version4(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
        version.preprocess();
        testSampleValues();

        testManyChunksSum(VERSION.FOUR, dataSingle, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
        testManyChunksSum(VERSION.FOUR, data2010, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
    }

    @Test
    public void testVersion5() {

        version = new Version5(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
        version.preprocess();
        testSampleValues();

        testManyChunksSum(VERSION.FIVE, dataSingle, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);
        testManyChunksSum(VERSION.FIVE, data2010, MIN_CHUNK_COLUMNS, MAX_CHUNK_COLUMNS, MIN_CHUNK_ROWS, MAX_CHUNK_ROWS);

        /* Timing code for writeup
        for (int cutoff=100000; cutoff>=10; cutoff-=100){
            long start = System.nanoTime();

            Version5 v = new Version5(data2010, MAX_CHUNK_COLUMNS, MAX_CHUNK_ROWS);
            v.SEQUENTIAL_CUTOFF = cutoff;
            v.preprocess();
            testChunksSum(v, MAX_CHUNK_COLUMNS, MAX_CHUNK_ROWS);
            System.out.println("Cutoff: " + cutoff + " took " + ((System.nanoTime() - start) / 1000000.0) + " milliseconds to build counts");
        }
        */

    }

    // Tests against known values given by Tyler Robison in an email
    // Thanks Tyler, this was very helpful!
    // Works for (COLUMNS, ROWS) = (20, 25)
    private void testSampleValues() {
        testSampleValue(1, 1, 5, 4, 1360301, 0.44f);
        testSampleValue(1, 12, 9, 25, 710231, 0.23f);
        testSampleValue(9, 1, 20, 13, 310400795, 99.34f);
        testSampleValue(1, 1, 20, 25, 312471327, 100.0f);
        testSampleValue(9, 1, 11, 25, 52392739, 16.77f);
        testSampleValue(1, 1, 20, 4, 36493611, 11.68f);
    }


    /**
     * Creates many sizes of Versions and tests the chunkSum on them.
     * NOTE: This can take a LONG, LONG time to run, but it's very thorough.
     *
     * @param vNum    version number specified by enum
     * @param d       dataset to use
     * @param minCols minimum number of columns in this QueryVersion
     * @param maxCols max columns in this QueryVersion
     * @param minRows min rows in this QueryVersion
     * @param maxRows max rows in this QueryVersion
     */
    private void testManyChunksSum(VERSION vNum, CensusData d, int minCols, int maxCols, int minRows, int maxRows) {
        for (int c = minCols; c <= maxCols; c++) {
            for (int r = minRows; r <= maxRows; r++) {
                switch (vNum) {
                    case ONE:
                        version = new Version1(d, c, r);
                        break;
                    case TWO:
                        version = new Version2(d, c, r);
                        break;
                    case THREE:
                        version = new Version3(d, c, r);
                        break;
                    case FOUR:
                        version = new Version4(d, c, r);
                        break;
                    case FIVE:
                        version = new Version5(d, c, r);
                        break;
                }
                version.preprocess();
                testChunksSum(version, c, r);
            }
        }
    }

    /**
     * Breaks the grid into many sized chunks and tests that the sums of their individual populations equals
     * the total population (and within a reasonable percentage of 100%).
     * NOTE: This can take a LONG time to run, but it's very thorough.
     *
     * @param v       Version object to sum chunks of
     * @param columns number of columns in the grid
     * @param rows    number of rows in the grid
     */
    private void testChunksSum(QueryVersion v, int columns, int rows) {
        Pair<Integer, Float> totalPair = v.query(1, 1, columns, rows);
        int totalPop = totalPair.getElementA();
        float totalPercent = totalPair.getElementB();

        for (int rStep = 1; rStep <= rows; rStep++) {
            for (int cStep = 1; cStep <= columns; cStep++) {
                int sumPop = 0;
                float sumPercent = 0;
                for (int c = 1; c <= columns; c += cStep + 1) {
                    for (int r = 1; r <= rows; r += rStep + 1) {
                        Pair<Integer, Float> tempPair = v.query(c, r, Math.min(c + cStep, columns), Math.min(r + rStep, rows));
                        sumPop += tempPair.getElementA();
                        sumPercent += tempPair.getElementB();
                    }
                }
                assertEquals(totalPop, sumPop);
                // Check that percent is within 0.1 percent times the max of rows or columns
                assert (Math.abs(totalPercent - sumPercent) < (0.1 * Math.max(columns, rows)));
            }
        }
    }

    // Tests a single sample value against a known population and percentage
    private void testSampleValue(int w, int s, int e, int n, int t, float p) {
        Pair<Integer, Float> result = version.query(w, s, e, n);
        assertEquals(new Integer(t), result.getElementA());
        assert (Math.abs(result.getElementB() - p) < 1.0f);
    }
}
