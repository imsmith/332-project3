/**
 * Timing test code for writeup question 8
 *
 * @authors Ian Smith and Tim Plummer
 */

import org.junit.Before;
import org.junit.Test;

public class TestQ8b {
    public QueryVersion version1;
    public QueryVersion version2;
    public QueryVersion version3;
    public QueryVersion version4;

    public CensusData data2010;
    public CensusData dataSingle;
    
    private final int[] queries = {5, 10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000};

    // Default rows and columns for this test
    // (the sample values in testSampleValues() depend on these numbers)
    private final int TEST_SAMPLE_COLUMNS = 20;
    private final int TEST_SAMPLE_ROWS = 25;

    @Before
    public void setup() {
        // Parse the files
        data2010 = PopulationQuery.parse("CenPop2010.txt");
        dataSingle = PopulationQuery.parse("CenPopSingle.txt");
    }

    @Test
    public void testVersion24() {
    	for(int i=0; i<=1000; i+=1)
    	{
	        version2 = new Version1(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
	        version4 = new Version3(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
	        long start1 = System.nanoTime();
        	version2.preprocess();
        	long end1 = System.nanoTime()-start1;
        	long start2 = System.nanoTime();
        	version4.preprocess();
        	long end2 = System.nanoTime()-start2;
        	
	        
        	long time2 = timeXQueries(1, 1, 20, 25, version2, i);
	        long time4 = timeXQueries(1, 1, 20, 25, version4, i);
	        
	        System.out.println("\t" + i + "\t\t" + time2 + "\t\t" + time4 + "\t\t" + (end2-end1));
    	}
    }

    @Test
    public void testVersion13() {
        for(int i=0; i<=100; i+=1)
        {
            long start1 = System.nanoTime();
            version1 = new Version1(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
            version1.preprocess();
            long end1 = System.nanoTime()-start1;

            long start3 = System.nanoTime();
            version3 = new Version3(data2010, TEST_SAMPLE_COLUMNS, TEST_SAMPLE_ROWS);
            version3.preprocess();
            long end3 = System.nanoTime()-start3;



            long time1 = timeXQueries(1, 1, 20, 25, version1, i);
            long time3 = timeXQueries(1, 1, 20, 25, version3, i);

            double total1 = (time1 + end1) / 1000000.0;
            double total3 = (time3 + end3) / 1000000.0;
            System.out.println(i + "\t\t" + total1 + "\t\t" + total3);
        }
    }
    // Tests a single sample value against a known population and percentage
    private long timeXQueries(int w, int s, int e, int n, QueryVersion version, int x) {
        long start = System.nanoTime();
        for(int i=0; i<x; i++)
        {
        	version.query(w, s, e, n);
        }
        long time = System.nanoTime()-start;
        return time;
    }
}
