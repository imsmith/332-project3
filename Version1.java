
/**
 * @author Tim Plummer
 * @partner Ian Smith
 * Version 1 computes the population query sequentially
 */
public class Version1 extends QueryVersion
{
	/**
	 * Version constructor
	 * @param data Raw census data
	 * @param columns Number of columns in grid
	 * @param rows Number of rows in grid
	 */
	public Version1(CensusData data, int columns, int rows)
	{
		this.data = data;
		this.columns = columns;
		this.rows = rows;
	}
	
	/* (non-Javadoc)
	 * @see QueryVersion#preprocess()
	 */
	@Override
	public void preprocess()
	{
		// Start the edges as a single point from the data
		edges = new Rectangle(data.data[0].longitude, data.data[0].longitude, 
										data.data[0].latitude, data.data[0].latitude);
		for(CensusGroup group : data.data)
		{
			if(group == null) continue;
			// Enlarge the current edge by the new point
			edges = edges.encompass(new Rectangle(group.longitude, group.longitude, 
												group.latitude, group.latitude));
			// Add up the population on that point
			totalPop += group.population;
		}
	}

	/* (non-Javadoc)
	 * @see QueryVersion#query(int, int, int, int)
	 */
	@Override
	public Pair<Integer, Float> query(int w, int s, int e, int n)
	{
		int queryPop = 0;
		for(CensusGroup group : data.data)
		{
			if(group == null) break;
			// Transform the map coordinates to grid coordinates
			int x = toGrid(group.longitude, edges.left, edges.right, columns);
			int y = toGrid(group.latitude, edges.bottom, edges.top, rows);
			// Check that the population is contained in the grid
			if(x >= w && x <= e)
				if(y >= s && y <= n)
					queryPop += group.population;
		}
		
		// Return the computed data
		return new Pair<Integer, Float>(queryPop, (float)queryPop/(float)totalPop * 100.0f);
	}
}
