/**
 * Version3 class extends Version1, preprocesses the population in one pass of the census
 * data + O(rows x columns), and returns queries in O(1).
 *
 * @author Ian Smith
 * @partner Tim Plummer
 */
public class Version3 extends Version1 {

    /*
     * Array of arrays, where each element is a single grid position [column-1][row-1].
     * Grid element g stores the total population in the rectangle whose upper-left
     * is the North-West corner of the country and the lower-right corner is g.
     * Initialized to all zeros by Java because it's a class member.
     */
    protected int[][] accumulatedPopGrid;

    /**
     * Constructor
     * Creates a new empty Version3 object with an empty 2-D array the size of columns x rows.
     *
     * @param data    census data
     * @param columns number of columns in grid
     * @param rows    number of rows in grid
     */
    public Version3(CensusData data, int columns, int rows) {
        super(data, columns, rows);
        this.accumulatedPopGrid = new int[columns][rows];
    }

    /**
     * Preprocesses the census data to populate a 2-D array to allow for O(1) queries.
     */
    @Override
    public void preprocess() {
        super.preprocess();

        calculatePopGrid(accumulatedPopGrid);
        calculateAccumulatedPopGrid(accumulatedPopGrid);
    }

    /**
     * Queries this object to return the population within the rectangular grid, and the percentage of the total
     * bounded by West, South, East, and North divisions.
     *
     * @param w West-most grid division query bounds
     * @param s South-most grid division query bounds
     * @param e East-most grid division query bounds
     * @param n North-most grid division query bounds
     * @return Pair of Integer and Float, where Integer is the population bounded by the specified rectangle coordinates
     *         and Float is the percentage of the total population
     */
    @Override
    public Pair<Integer, Float> query(int w, int s, int e, int n) {
        // decrement coordinates so they match up with array indexes
        n--;
        s--;
        e--;
        w--;

        /* Take the value in the bottom-right corner of the query rectangle.
         * Subtract the value just above the top-right corner of the query rectangle 
         * (or 0 if that is outside the grid).
         * Subtract the value just left of the bottom-left corner of the query rectangle 
         * (or 0 if that is outside the grid).
         * Add the value just above and to the left of the upper-left corner of the 
         * query rectangle (or 0 if that is outside the grid).
         */
        int br = accumulatedPopGrid[e][s];
        int tr = (n == rows - 1) ? 0 : accumulatedPopGrid[e][n + 1];
        int bl = (w == 0) ? 0 : accumulatedPopGrid[w - 1][s];
        int tl = (w == 0 || n == rows - 1) ? 0 : accumulatedPopGrid[w - 1][n + 1];

        int pop = br - tr - bl + tl;

        return new Pair<Integer, Float>(pop, (float) pop / (float) totalPop * 100.0f);
    }

    /**
     * Fills the 2-D array with the populations at each grid division with a single pass over the census data.
     *
     * @param popArr The 2-D array to populate
     */
    protected void calculatePopGrid(int[][] popArr) {
        for (CensusGroup group : data.data) {
            if (group == null) continue;
            //subtract 1 from toGrid() answer because our array is 0-indexed
            int x = toGrid(group.longitude, edges.left, edges.right, columns) - 1;
            int y = toGrid(group.latitude, edges.bottom, edges.top, rows) - 1;
            popArr[x][y] += group.population;
        }
    }

    /**
     * Processes the 2-D array in O(rows x columns) to set the population at each element to the total
     * population that is neither East or South of the element.
     *
     * @param popArr The 2-D array to process
     */
    protected void calculateAccumulatedPopGrid(int[][] popArr) {
        for (int i = 0; i < columns; i++) {
            for (int j = rows - 1; j >= 0; j--) {
                // If not on the Western border add the element to the West
                if (i != 0) {
                    popArr[i][j] += popArr[i - 1][j];
                }
                // If not on the Northern border add the element to the North
                if (j != rows - 1) {
                    popArr[i][j] += popArr[i][j + 1];
                }
                // If not on a North or West border, add the element to the NorthWest
                if (i != 0 && j != rows - 1) {
                    popArr[i][j] -= popArr[i - 1][j + 1];
                }
            }
        }
    }
}
